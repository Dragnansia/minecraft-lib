## Minecraft Lib

Minecraft library to download, connect and start minecraft instance.

### Todo
* [ ] Downloads files (one by one & parallel)
* [ ] App config
* [ ] Create instances
* [ ] Start instance
* [ ] Download ModPacks / Mods -- (CurseForge, Modrinth)
* [ ] Install ModPack from zip
* [ ] ...
