#[cfg(test)]
mod tests {
    use crate::api::{launcher_meta::LauncherMeta, package::Package};

    #[test]
    fn parse_launcher_meta() {
        let client =
            reqwest::blocking::get("https://launchermeta.mojang.com/mc/game/version_manifest.json")
                .expect("Can't download version_manifest");
        let body = client.text().expect("Can't convert response to bytes");
        let res =
            serde_json::from_str::<LauncherMeta>(&body).expect("Can't convert to LauncherMeta");

        assert!(res.versions.len() > 0);
    }

    #[test]
    fn parse_package() {
        let client =
            reqwest::blocking::get("https://launchermeta.mojang.com/mc/game/version_manifest.json")
                .expect("Can't download version_manifest");
        let body = client.text().expect("Can't convert response to bytes");
        let launcher_meta =
            serde_json::from_str::<LauncherMeta>(&body).expect("Can't convert to LauncherMeta");

        let package_url = &launcher_meta.versions[0].url;
        let body = reqwest::blocking::get(package_url)
            .expect("Can't download package")
            .text()
            .expect("Can't convert response to bytes");
        let package = serde_json::from_str::<Package>(&body);

        assert!(package.is_ok());
    }
}
