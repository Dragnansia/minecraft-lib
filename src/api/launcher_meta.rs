use chrono::prelude::*;
use serde::{Deserialize, Serialize};

/// Information get from `https://launchermeta.mojang.com/mc/game/version_manifest.json`
/// All versions information and who are the latest for release and snapshot
#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct LauncherMeta {
    pub latest: Latest,
    pub versions: Vec<Version>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Latest {
    pub release: String,
    pub snapshot: String,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Version {
    pub id: String,
    #[serde(rename = "type")]
    pub _type: String,
    pub url: String,
    pub time: DateTime<Utc>,
    #[serde(rename = "releaseTime")]
    pub release_time: DateTime<Utc>,
}
