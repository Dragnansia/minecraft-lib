use chrono::prelude::*;
use serde::{Deserialize, Serialize};

/// All information for specific version of minecraft
#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Package {
    #[serde(rename = "type")]
    pub _type: String,
    pub time: DateTime<Utc>,
    #[serde(rename = "releaseTime")]
    pub release_time: DateTime<Utc>,
    #[serde(rename = "minimumLauncherVersion")]
    pub min_launcher_version: u16,
    #[serde(rename = "mainClass")]
    pub main_class: String,
    pub logging: Logging,
    pub libraries: Vec<Library>,
    #[serde(rename = "javaVersion")]
    pub java_version: JavaVersion,
    pub id: String,
    pub downloads: Downloads,
    #[serde(rename = "complianceLevel")]
    pub compliance_level: u8,
    pub assets: String,
    #[serde(rename = "assetIndex")]
    pub asset_index: Object,
    pub arguments: Arguments,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Arguments {
    pub game: Vec<Args>,
    pub jvm: Vec<Args>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Args {
    String(String),
    Rule(Rule),
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Rule {
    pub rules: Vec<RuleArgs>,
    pub value: RuleValue,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RuleArgs {
    pub action: String,
    pub features: Option<RuleFeatures>,
    pub os: Option<RuleOs>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RuleFeatures {
    pub is_demo_user: Option<bool>,
    pub has_custom_resolution: Option<bool>,
    pub has_quick_plays_support: Option<bool>,
    pub is_quick_play_singleplayer: Option<bool>,
    pub is_quick_play_multiplayer: Option<bool>,
    pub is_quick_play_realms: Option<bool>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RuleOs {
    name: Option<String>,
    arch: Option<String>,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum RuleValue {
    String(String),
    List(Vec<String>),
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Object {
    pub id: Option<String>,
    pub sha1: String,
    // File to download size
    pub size: u64,
    // Total size to download with this file informations
    #[serde(rename = "totalSize")]
    pub total_size: Option<u64>,
    pub url: String,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Downloads {
    pub client: Object,
    pub client_mappings: Object,
    pub server: Object,
    pub server_mappings: Object,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct JavaVersion {
    pub component: String,
    #[serde(rename = "majorVersion")]
    pub major_version: u8,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Library {
    pub name: String,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Logging {
    client: Client,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Client {
    pub argument: String,
    pub file: File,
    #[serde(rename = "type")]
    pub _type: String,
}

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct File {
    pub id: String,
    pub sha1: String,
    pub size: u64,
    pub url: String,
}
